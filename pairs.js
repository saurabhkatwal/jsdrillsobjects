let obj = require("./test/testObject");
let values = require("./values");

function pairs(obj) {
    let valuesList = values(obj);
    // console.log(valuesList);
    let keysList=Object.keys(obj);
    let validKeysList=keysList.filter(key=>typeof obj[key]!=="function");
    let result=[];
    for(let i=0;i<validKeysList.length;i++){
        let temp=[];
        temp.push(validKeysList[i]);
        temp.push(valuesList[i]);
        // result+=[validKeysList[i],valuesList[i]];
        result.push(temp);
    }
    return result;
}
module.exports=pairs;