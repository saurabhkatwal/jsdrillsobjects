function values(obj){
let keyValues=Object.keys(obj);
let result=keyValues.filter(keyValue=>{
    return typeof obj[keyValue]!=="function";
}).map(keyValue=>{
    return obj[keyValue];
});
return result;
}
module.exports=values;
