// let obj = {flavor: "chocolate"};
// const testObject = {
//     name: 'Bruce Wayne',
//     age: 36,
//     location: 'Gotham',
//     say: function () {
//         console.log("hello");
//     }
// };
// let testObject = require("./test/testObject");
// let defaultProps = {
//     "butler": "Alfred",
//     "car": "batmobile",
//     age: 36
// };
// let defaultPropertiesList = Object.keys(defaultProps);
// console.log(defaultPropertiesList);
// defaultPropertiesList.forEach(property => {
//     if(testObject[property]===undefined){
//         testObject[property]=defaultProps[property];
//     }
// })
// console.log(testObject);



// let obj = require("./test/testObject");
function defaults(obj,defaultProps){
let defaultPropertiesList=Object.keys(defaultProps);
defaultPropertiesList.forEach(property=>{
    if(obj[property]===undefined){
        obj[property]=defaultProps[property];
    }
})
return obj;
}
// defaults(testObject,defaultProps);
// defaults(obj,{flavor: "vanilla", sprinkles: "lots"})
module.exports=defaults;