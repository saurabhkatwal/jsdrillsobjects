let obj=require("./test/testObject");
function invert(obj){
let obj2={};
let keyValues=Object.keys(obj);
let validKeyValues=keyValues.filter(keyValue=>typeof obj[keyValue]!=="function");
let validValues=validKeyValues.map(validKeyValue=>String(obj[validKeyValue]));
for(let i=0;i<validValues.length;i++){
    obj2[validValues[i]]=validKeyValues[i];
}
return obj2;
}
// let reversedObject = invert(obj);

module.exports=invert;