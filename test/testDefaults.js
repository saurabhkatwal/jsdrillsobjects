let obj = require("./testObject");
let defaults = require("../defaults");
let defaultProps = {
    "butler": "Alfred",
    "car": "batmobile",
    age: 36,
    name:"Bruce Wayne"
};
let modifiedObject = defaults(obj, defaultProps);
console.log(modifiedObject);