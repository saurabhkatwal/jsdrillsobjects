// let testObject = require("./test/testObject");

let validKeyValues;
function mapObject(obj,cb) {
    let keyValues = Object.keys(obj);
    validKeyValues = keyValues.filter(keyValue => typeof obj[keyValue] !== "function");
    // let transformedValues = validKeyValues.map(validKeyValue=>obj[validKeyValue]+2);
    let transformedValues=[];
    for(let i=0;i<validKeyValues.length;i++){
        transformedValues.push(cb(obj,validKeyValues[i]));
    }
    return transformedValues;
}
module.exports.mapObject=mapObject;
module.exports.validKeyValues=validKeyValues;

// let result = mapObject(testObject,myFunction);
// console.log(result);